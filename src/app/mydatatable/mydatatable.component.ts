import { Component, OnInit } from '@angular/core';
import { ObjectDataTableAdapter } from '@alfresco/adf-core';

@Component({
  selector: 'app-mydatatable',
  templateUrl: './mydatatable.component.html',
  styleUrls: ['./mydatatable.component.css']
})
export class MydatatableComponent implements OnInit {

  data = new ObjectDataTableAdapter(
    [
      {
        id: 1, 
        firstName: 'Name #1', 
        lastName: 'Lastname #1', 
        icon: 'material-icons://folder_open'
      },
      {
        id: 2, 
        firstName: 'Name #2', 
        lastName: 'Lastname #2', 
        icon: 'material-icons://accessibility'
      },
      {
        id: 3, 
        firstName: 'Name #3', 
        lastName: 'Lastname #3', 
        icon: 'material-icons://alarm'
      }
    ]
  );

  constructor() { }

  ngOnInit(): void {
  }

}
