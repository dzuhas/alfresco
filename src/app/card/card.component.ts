import { Component, OnInit } from '@angular/core';
import { CardViewComponent } from '@alfresco/adf-core';
import { CardViewTextItemModel,CardViewMapItemModel,CardViewDateItemModel,CardViewDatetimeItemModel,CardViewBoolItemModel,CardViewIntItemModel,CardViewFloatItemModel,CardViewKeyValuePairsItemModel,CardViewSelectItemModel,CardViewArrayItemModel } from '@alfresco/adf-core';
import { of } from 'rxjs';
@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  properties = [
    new CardViewTextItemModel({
        label: 'Name',
        value: 'Spock',
        key: 'name',
        default: 'default bar' ,
        multiline: false,
        icon: 'icon'
    }),
    new CardViewMapItemModel({
        label: 'My map',
        value: new Map([['999', 'My Value']]),
        key: 'map',
        clickable: true,
    }),
    new CardViewDateItemModel({
        label: 'Date of birth',
        value: new Date("2000-09-09"),
        key: 'date-of-birth',
        format: 'MM dd YYYY',
        editable: true
    }),
    new CardViewBoolItemModel({
        label: 'Vulcanian',
        value: true,
        key: 'vulcanian',
        default: false
    }),
    new CardViewIntItemModel({
        label: 'Intelligence',
        value: 213,
        key: 'intelligence',
        default: 1
    }),
    new CardViewFloatItemModel({
        label: 'Mental stability',
        value: 9.9,
        key: 'mental-stability',
        default: 0.0
    }),
    new CardViewKeyValuePairsItemModel({
        label: 'Variables',
        value: [{ name: 'John', value: '100' }],
        editable:true,
        key: 'key-value-pairs'
    }),
    new CardViewSelectItemModel({
        label: 'Select box',
        value: 'one',
        options$: of([{ key: 'one', label: 'One' }, { key: 'two', label: 'Two' }]),
        key: 'select'
     })
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
