import { Component, OnInit } from '@angular/core';
import { CommentModel } from '@alfresco/adf-core';

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.css']
})
export class CommentListComponent implements OnInit {
  

    comments: CommentModel[] = [
      {
        id: 1,
        message: 'Comment number 1',
        created: new Date(),
        createdBy: {
          id: '1',
          email: 'john.doe@alfresco.com',
          firstName: 'John',
          lastName: 'Doe'
        },
        isSelected: false
      },
      {
        id: 2,
        message: 'Comment number 2',
        created: new Date(),
        createdBy: {
          id: '2',
          email: 'jane.doe@alfresco.com',
          firstName: 'Jane',
          lastName: 'Doe'
        },
        isSelected: false
      }
    ];
  
    onClickCommentRow(comment: CommentModel) {
      console.log('Clicked row: ', comment);
    }
  constructor() { }

  ngOnInit(): void {
  }

}
